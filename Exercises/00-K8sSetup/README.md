# Kubernetes: The Essentials
# Hands-on Exercises #0

### Objective

In these exercises we will connect and initialize the lab environment

### Parts

[Part 1: Connect to Labs](Part-01-ConnectToLabs.md)

[Part 2: K8s Setup](Part-02-K8sSetup.md)

[Part 3: kubectl](Part-03-kubectl.md)

Return to the course [Table of Content](../README.md)
