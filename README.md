# Azure Kubernetes Services

### Lab Exercises

Here is a quick link to the [Exercise Page](Exercises)

### Overview
For IT professionals, developers, software engineers, and DevOps practitioners – microservices training provides the technical practices and tooling fundamentals necessary to begin realizing the benefits of microservices as a foundation for IT architecture, software engineering, and service/release delivery. The workshop includes many hands-on exercises that give you real-world practice on the engineering tools and skills a team needs in order to realistically implement your own flavor of Microservices architecture patterns so you can address the team needs of your own organization. Whether you want to create new services, decouple a few services from your overall architecture, or refactor an entire monolithic architecture into a microservice design pattern, this course quickly teaches you the practical toolset and skills to get up and running with microservices in your own systems. Loosely coupled components and services allow teams to deploy more freely and independently, with less risk to the architecture.

### Learning Objectives
- Adopt, plan or improve your transition to microservices
- Map technical practices to the business strategy behind microservices
- Navigate different tools for enabling microservices and how to use them and
enable more automated testing and self-service QA
- Communicate with stakeholders, management, and teams regarding needs and
expectations around microservices
- Get hands-on practice with Docker, Kubernetes, and Jenkins tools for core
microservices architecture
- Get hands-on practice with the toolchain in our real-world application labs
- Build more mature DevOps practices through adoption
- Understand how to refactor monolithic systems into more modular, component- based systems
- Apply microservice use cases to continuous integration, delivery, and testing

### Target Audience
- Network engineers
- System and software architects
- Developers and engineers
- Testers and QA teams
- Release engineers
- IT operations staff
- Application Developers and Managers
- Operations Developers
- Technology Leaders & Managers
- Site reliability engineers
- DevOps practitioners and engineers
- DBAs and data engineering teams
- Information Security Pros

### Skill Level
Beginner / Intermediate

### Pre-Requisites
This is a hands-on coding and lab-intensive training course. Professionals who take this course should have some familiarity with engineering, containers, introductory Kubernetes, and basic cloud computing before attending. If you are an engineer interested to learn about Microservices, this session is for you.

### Course Outline
#### __Part 1 : Intro to Microservices__
1. Define Microservices
2. Amazon Web Services Case Study (SOA / Microservices)
  - Problem: Scaling the Organization and the ‘Big ball of mud’
  - Conway’s Law
  - Service Oriented Architecture
  - Forced Self Service Mandate
  - Result: Amazon dominance of cloud
  - Result: High velocity at scale
3. Intro to Containers (encapsulation of a service)
  - What is Docker
  - Exercise: Install Docker
  - Exercise: Docker Hello World o Docker ecosystem
  - Docker concepts
  - Benefits
    - Configure once, run everywhere
  - VM’s vs Container use cases
    - Databases & stateless workloads
  - Container encapsulation/ideal use cases
    - Encapsulation
    - Speed
    - Efficiency of computing resources
  - Docker Architecture
  - Exercise: Docker 101 - Web App
  - Docker Images
  - Exercise: Stateless Web App
  - Data Volumes
  - Exercise: Docker 201
  - Compose Multi-tier app
  - Docker Security
  - Continuous Integration
    - Canary Release
    - Blue Green Deployment § Rolling Update
4. Microservice challenge : Continuous Integration Service
  - CI/CD Orchestration
    - Jenkins
  - Exercise: Trigger build/tests from change

#### __Part 2 : Microservices in Development__
1. The12FactorApp
  - Why 12 Factors
  - What 12 Factors
2. Microservice challenge : Image repository
  - Private repository Service § Registry
    - Quay.io
    - Harbor
  - Public repository
    - Docker Hub
  - Exercise : Submit image to service
  - Exercise : Pull image from service
3. Microservice challenge : Service discovery
  - Consul
  - CoreDNS
4. Microservice challenge : Secrets
  - Vault
  - Kubernetes Secrets

#### __Part 3 : Microservices in Production__
1. Intro to Kubernetes
  - Prerequisites
  - Containers
  - Linux Kernel Features
  - Container User Experience
  - New Container Capabilities
  - Gaps using Containers in Production
2. Exercise : Kubernetes 100 : Hello World!
3. CoreConcepts
  - Cluster Orchestration
  - Originated at Google
  - Open Source
  - Benefits
  - Design Principles
4. Kubernetes Architecture
  - Master / Node
  - Kubectl
  - Kubelet
  - Kube-Proxy
  - Etcd
  - Exercise : Kubernetes 101 : Stateless web app
5. KubernetesObjects
  - Namespaces
  - Labels
  - Pods
  - ReplicaSet
  - Deployments
  - Services
  - Other Objects
6. Kubernetes PodSpec
  - InitContainers
  - Resource Quota
  - Volumes
  - Persistent Volumes
  - ConfigMaps
  - Secrets
7. Exercise : Kubernetes 201 : Guestbook app

#### __Part 4 : Microservices in Practice__
1. Exercise : Resolve service with DNS
2. Exercise : Kubernetes - Store database credentials in cluster
3. Exercise : Wordpress on Kubernetes
4. Exercise : Customize Microservice App
5. Exercise : Scale app for simulated demand
6. InSummary
  - Why Microservices?
  - Review of Microservice Challenges
